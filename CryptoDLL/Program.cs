﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoDLL
{
    class Program
    {

        private static Random random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        static void Main(string[] args)
        {
            RSA r = new RSA();
            Random rand = new Random();
            byte[] a;
            string s;
            string mensagem;
            int i;
            const string chars = "abcdefghijklmnopqrtuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ";
            while (true)
            {
                int length = rand.Next(1, 30);
                mensagem = RandomString(length);
                Console.WriteLine("> INICIO ==========================");
                Console.WriteLine("encriptando " + mensagem);
                a = r.Encrypt(mensagem);
                Console.WriteLine("encriptado : ");
                Console.Write("[");
                i = 0;
                while (i < a.Length)
                {
                    Console.Write(a[i] + (i == a.Length-1? "" : " "));
                    i++;
                }
                Console.WriteLine("]");
                s = r.Decrypt(a);
                Console.WriteLine("descriptografado " + s);
                Console.WriteLine("> FIM ==========================");

            }
            Console.ReadKey();
            //teste commit
        }
    }
}
