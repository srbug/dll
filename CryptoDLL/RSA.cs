﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace CryptoDLL
{
    public class RSA
    {
        struct GoString
        {
            public IntPtr p;
            public int n;
        }


        public struct GoSlice
        {
            public IntPtr data;
            public int len;
            public int cap;
        };


        public byte[] Encrypt(string encrypt)
        {
            byte[] res;
            IntPtr output = IntPtr.Zero;
            var x = Encoding.UTF8.GetBytes(encrypt);
            int i;
            /*
            Setar projeto para 64bits e não any cpu ou x86
            */
            i = RsaEncrypt(x, ref output);
            res = new byte[i];
            Marshal.Copy(output, res, 0, i);
            FreeMemory(ref output);
            return res;
        }


        public string Decrypt(byte[] decrypt)
        {
            string res = "demo";
            IntPtr output = IntPtr.Zero;
            var x = Encoding.UTF8.GetBytes(res);
            int i;
            byte[] dado;
            /*
                Setar projeto para 64bits e não any cpu ou x86
            */
            i = RsaDecrypt(decrypt, decrypt.Length, ref output);
            dado = new byte[i];
            Marshal.Copy(output, dado, 0, i);
            FreeMemory(ref output);
            res = Encoding.UTF8.GetString(dado);
            return res;
        }
        [DllImport("RSA.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int RsaEncrypt([In] byte[] data, ref IntPtr output);

        [DllImport("RSA.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int RsaDecrypt([In] byte[] data,int sizeofData, ref IntPtr output);

        [DllImport("RSA.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void FreeMemory(ref IntPtr cPointer);
        
    }
}
