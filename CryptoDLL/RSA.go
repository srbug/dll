package main

/*
#include <stdlib.h>
*/
import "C"
import (
	"encoding/pem"
	"crypto/x509"
	"crypto/rsa"
	"crypto/rand"
	"fmt"
	"unsafe"
)

var privateKey = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQ
WMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jE0NQ+Z+zEdhUTooNR
aY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4gwQco1KRMDSmXSMkDwIDAQAB
AoGAfY9LpnuWK5Bs50UVep5c93SJdUi82u7yMx4iHFMc/Z2hfenfYEzu+57fI4fv
xTQ//5DbzRR/XKb8ulNv6+CHyPF31xk7YOBfkGI8qjLoq06V+FyBfDSwL8KbLyeH
m7KUZnLNQbk8yGLzB3iYKkRHlmUanQGaNMIJziWOkN+N9dECQQD0ONYRNZeuM8zd
8XJTSdcIX4a3gy3GGCJxOzv16XHxD03GW6UNLmfPwenKu+cdrQeaqEixrCejXdAF
z/7+BSMpAkEA8EaSOeP5Xr3ZrbiKzi6TGMwHMvC7HdJxaBJbVRfApFrE0/mPwmP5
rN7QwjrMY+0+AbXcm8mRQyQ1+IGEembsdwJBAN6az8Rv7QnD/YBvi52POIlRSSIM
V7SwWvSK4WSMnGb1ZBbhgdg57DXaspcwHsFV7hByQ5BvMtIduHcT14ECfcECQATe
aTgjFnqE/lQ22Rk0eGaYO80cc643BXVGafNfd9fcvwBMnk0iGX0XRsOozVt5Azil
psLBYuApa66NcVHJpCECQQDTjI2AQhFc1yRnCU/YgDnSpJVm1nASoRUnU8Jfm3Oz
uku7JUXcVpt08DFSceCEX9unCuMcT72rAQlLpdZir876
-----END RSA PRIVATE KEY-----
`)

var publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlOJu6TyygqxfWT7eLtGDwajtN
FOb9I5XRb6khyfD1Yt3YiCgQWMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76
xFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4
gwQco1KRMDSmXSMkDwIDAQAB
-----END PUBLIC KEY-----
`)

//export RsaEncrypt
func RsaEncrypt(Input *C.char, Output **C.char) C.int {
	origData := C.GoString((*C.char)(unsafe.Pointer(Input)));
	block, _ := pem.Decode(publicKey);
	pubInterface, _ := x509.ParsePKIXPublicKey(block.Bytes)
	pub := pubInterface.(*rsa.PublicKey);
	var rsp, _ = rsa.EncryptPKCS1v15(rand.Reader, pub, []byte(origData));
	firstAddress := (*C.char)(unsafe.Pointer(&rsp[0]));
	*Output = C.CString(fmt.Sprintf("%s", C.GoStringN(firstAddress, C.int(len(rsp)))));
	return C.int(len(rsp));
}


//export RsaDecrypt
func RsaDecrypt(Input *C.char, _size C.int, Output **C.char) C.int {
 origData := C.GoStringN((*C.char)(unsafe.Pointer(Input)), _size);
 ciphertext := []byte(origData);
 block, _ := pem.Decode(privateKey);
 priv, _ := x509.ParsePKCS1PrivateKey(block.Bytes);
 var rsp, _ = rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext);
 firstAddress := (*C.char)(unsafe.Pointer(&rsp[0]));
 *Output = C.CString(fmt.Sprintf("%s", C.GoStringN(firstAddress, C.int(len(rsp)))));
 return C.int(len(rsp));
}


//export FreeMemory
func FreeMemory(Output **C.char) {
	C.free(unsafe.Pointer(*Output));
}

//export makedll
func makedll(){}

func main(){}